@extends('layouts.dashboard')
@section('title')
    Бюджеты
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <a href="{{ route('budget.create') }}" class="btn btn-success btn-floating btn-outline pull-right">
                        Добавить
                    </a>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Наименования</th>
                                <th>Действия</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($budgets as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->title }}</td>
                                    <td>
                                        <a href="{{ route('budget.edit', ['budget' => $item]) }}" class="btn btn-warning">
                                            Изменить
                                        </a>
                                        <button type="button" class="btn btn-danger btn-outline"
                                                data-toggle="modal" data-target="#deleteBudgetModal"
                                                data-id="{{ $item->id }}"
                                                onclick="deleteBudget(this)">
                                            Удалить
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteBudgetModal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('budget.destroy') }}" method="post">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel9">Системное уведомление</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="zmdi zmdi-close"></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" name="budget" id="deleteBudget">
                            <p>Вы действительно хотите удалить?</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-outline" data-dismiss="modal">Закрыть</button>
                        <button type="submit" class="btn btn-danger">Удалить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@push('page-js')
    <script src="{{ asset('js/category.js') }}"></script>
@endpush