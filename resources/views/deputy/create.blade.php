@extends('layouts.dashboard')
@section('title')
    Добавить депутат
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('deputy.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name">Фио</label>
                            <input type="text" name="fio" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Инфо</label>
                            <input type="text" name="desc" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Программа</label>
                            <textarea name="info" id="" cols="30" rows="10"
                                      class="form-control my-editor"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="image">Изоброжения</label>
                                    <input type="file" name="image" class="form-control">
                                </div>
                            </div>
                        </div>
                        <a href="{{ route('deputy.index') }}" class="btn btn-danger">Назад</a>
                        <button type="submit" class="btn btn-success pull-right">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@push('page-js')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="{{ asset('js/tinymce.js') }}"></script>
@endpush