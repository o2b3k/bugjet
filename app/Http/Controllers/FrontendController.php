<?php

namespace App\Http\Controllers;

use App\News;
use App\Page;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
        return view('frontend.index');
    }

    public function viewPage($id)
    {
        $page = Page::whereId($id)->first();
        return view('frontend.view_page', ['page' => $page]);
    }

    public function viewCategory($id)
    {
        $news = News::whereCategoryId($id)->get();
        return view('frontend.view_category', ['news' => $news]);
    }

    public function viewNews($id)
    {
        $news = News::whereId($id)->first();
        return view('frontend.view_news', ['news' => $news]);
    }
}
