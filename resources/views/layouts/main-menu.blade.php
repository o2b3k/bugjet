<nav class="main-menu">
    <ul class="nav metismenu">
        <li class="sidebar-header"><span>Главная</span></li>
        <li><a href="{{ route('profile.deputy') }}"><i class="icon dripicons-user-group"></i><span>Кандидаты и программы</span></a>
        <li><a href="{{ route('profile.budget') }}"><i class="icon dripicons-pamphlet"></i><span>Утверждение местного бюджета</span></a></li>
        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();" role="menuitem">
                    <i class="icon dripicons-lock" aria-hidden="true"></i>Выход</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form></li>
        @if(Auth::user()->role == "admin")
            <li class="sidebar-header"><span>Админстратор</span></li>
            <li class="nav-dropdown active">
                <a class="has-arrow" href="#" aria-expanded="false"><i class="icon dripicons-checklist"></i><span>Кастомизация</span></a>
                <ul class="collapse nav-sub" aria-expanded="true">
                    <li><a href="{{ route('users.index') }}">Пользователи</a></li>
                    <li><a href="{{ route('category.index') }}"><span>Категория</span></a></li>
                    <li><a href="{{ route('news.index') }}"><span>Новости</span></a></li>
                    <li><a href="{{ route('page.index') }}"><span>Страницы</span></a></li>
                    <li><a href="{{ route('deputy.index') }}"><span>Депутаты</span></a></li>
                    <li><a href="{{ route('budget.index') }}"><span>Бюджет</span></a></li>
                    <li><a href="{{ route('menu.index') }}"><span>Меню</span></a></li>
                </ul>
            </li>
        @endif
    </ul>
</nav>