@extends('layouts.dashboard')
@section('title')
    Добавить новост
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('news.update', ['news' => $news]) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name">Заголовок</label>
                            <input type="text" name="title" class="form-control" value="{{ $news->title }}" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Текст</label>
                            <textarea name="desc" id="" cols="30" rows="10"
                                      class="form-control my-editor" required>
                                {{ $news->desc }}
                            </textarea>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="image">Изоброжения</label>
                                    <img src="{{ asset($news->image) }}" class="form-control" width="200" height="200">
                                    <input type="file" name="image" class="form-control">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="image">Категория</label>
                                    <select name="category_id" title="" class="form-control">
                                            <option value="{{ $news->category->id }}">{{ $news->category->name }}</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <a href="{{ route('news.index') }}" class="btn btn-danger">Назад</a>
                        <button type="submit" class="btn btn-success pull-right">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@push('page-js')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="{{ asset('js/tinymce.js') }}"></script>
@endpush