<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Votes
 *
 * @property int $id
 * @property string $vote
 * @property int|null $budget_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Budget[] $budget
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Votes whereBudgetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Votes whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Votes whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Votes whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Votes whereVote($value)
 * @mixin \Eloquent
 */
class Votes extends Model
{
    protected $table = 'votes';

    protected $fillable = ['vote', 'budget_id'];

    public function budget()
    {
        return $this->belongsTo(Budget::class);
    }
}