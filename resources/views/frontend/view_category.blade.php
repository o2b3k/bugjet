@extends('layouts.front')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                @foreach($news as $item)
                    <div class="col-sm-12 col-md-6 col-xl-3">
                        <div class="card">
                            <div class="card-content">
                                <img class="card-img-top img-fluid" src="{{ asset($item->image) }}">
                                <div class="card-body">
                                    <a href="{{ route('view.news', ['id' => $item->id]) }}"><h4
                                                class="card-title">{{ $item->title }}</h4></a>
                                    <p class="card-text">{!! \App\News::getExcerpt($item->desc) !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop