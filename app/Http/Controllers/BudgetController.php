<?php

namespace App\Http\Controllers;

use App\Budget;
use Illuminate\Http\Request;

class BudgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $budgets = Budget::all();
        return view('budget.index', ['budgets' => $budgets]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('budget.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->file('file') != null){
            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension();
            $fileName = str_random(5)."-".date('his')."-".str_random(3).".".$extension;
            $folderPath  = 'upload/files';
            $path = $file->move($folderPath , $fileName);
        }else{
            $path = null;
        }
        Budget::create([
            'title' => $request->input('title'),
            'text' => $request->input('text'),
            'file' => $path
        ]);

        return redirect()->route('budget.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Budget  $budget
     * @return \Illuminate\Http\Response
     */
    public function show(Budget $budget)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Budget  $budget
     * @return \Illuminate\Http\Response
     */
    public function edit(Budget $budget)
    {
        return view('budget.edit', ['budget' => $budget]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Budget  $budget
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Budget $budget)
    {
        if ($request->file('file') != null){
            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension();
            $fileName = str_random(5)."-".date('his')."-".str_random(3).".".$extension;
            $folderPath  = 'upload/files';
            $path = $file->move($folderPath , $fileName);
        }else{
            $path = $budget->file;
        }
        $budget->title = $request->input('title');
        $budget->text = $request->input('text');
        $budget->file = $path;
        $budget->save();

        return redirect()->route('budget.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Budget::destroy($request->input('budget'));
        return redirect()->route('budget.index');
    }
}
