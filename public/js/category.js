function editCategory(sender) {
    let id = $(sender).data('id');
    let name = $(sender).data('name');
    $('#editCategory').val(id);
    $('#categoryName').val(name);
}

function deleteCategory(sender) {
    let id = $(sender).data('id');
    $('#deleteCategory').val(id);
}

function deleteNews(sender) {
    let id = $(sender).data('id');
    $('#deleteNews').val(id);
}

function deletePage(sender) {
    let id = $(sender).data('id');
    $('#deletePage').val(id);
}

function deleteDeputy(sender) {
    let id = $(sender).data('id');
    $('#deleteDeputy').val(id);
}

function deleteBudget(sender) {
    let id = $(sender).data('id');
    $('#deleteBudget').val(id);
}

function deleteMenu(sender) {
    let id = $(sender).data('id');
    $('#deleteMenu').val(id);
}