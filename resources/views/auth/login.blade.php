<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Бюжет</title>
    <!-- ================== GOOGLE FONTS ==================-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500" rel="stylesheet">
    <!-- ======================= GLOBAL VENDOR STYLES ========================-->
    <link rel="stylesheet" href="{{ asset('assets/css/vendor/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/metismenu/dist/metisMenu.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/switchery-npm/index.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css') }}">
    <!-- ======================= LINE AWESOME ICONS ===========================-->
    <link rel="stylesheet" href="{{ asset('assets/css/icons/line-awesome.min.css') }}">
    <!-- ======================= DRIP ICONS ===================================-->
    <link rel="stylesheet" href="{{ asset('assets/css/icons/dripicons.min.css') }}">
    <!-- ======================= MATERIAL DESIGN ICONIC FONTS =================-->
    <link rel="stylesheet" href="{{ asset('assets/css/icons/material-design-iconic-font.min.css') }}">
    <!-- ======================= GLOBAL COMMON STYLES ============================-->
    <link rel="stylesheet" href="{{ asset('assets/css/common/main.bundle.css') }}">
    <!-- ======================= LAYOUT TYPE ===========================-->
    <link rel="stylesheet" href="{{ asset('assets/css/layouts/vertical/core/main.css') }}">
    <!-- ======================= MENU TYPE ===========================================-->
    <link rel="stylesheet" href="{{ asset('assets/css/layouts/vertical/menu-type/default.css') }}">
    <!-- ======================= THEME COLOR STYLES ===========================-->
    <link rel="stylesheet" href="{{ asset('assets/css/layouts/vertical/themes/theme-a.css') }}">
</head>

<body>
<div class="container">
    <form class="sign-in-form" action="{{ route('login') }}" method="POST">
        @csrf
        <div class="card">
            <div class="card-body">
                <h5 class="sign-in-heading text-center m-b-20">Вход</h5>
                <div class="form-group">
                    <label for="inputEmail" class="sr-only">Email</label>
                    <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email" required>
                </div>
                <div class="form-group">
                    <label for="inputPassword" class="sr-only">Пароль</label>
                    <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Пароль" required>
                </div>
                <div class="checkbox m-b-10 m-t-20">
                    <div class="custom-control custom-checkbox checkbox-primary form-check">
                        <input type="checkbox" class="custom-control-input" id="stateCheck1" checked="">
                        <label class="custom-control-label" for="stateCheck1">Запомни меня</label>
                    </div>
                    <a href="{{ route('password.request') }}" class="float-right">Забыли пароль?</a>
                </div>
                <button class="btn btn-primary btn-rounded btn-floating btn-lg btn-block" type="submit">Вход</button>
                <p class="text-muted m-t-25 m-b-0 p-0">У вас еще нет учетной записи?<a href="{{ route('register') }}">Регистрация</a></p>
            </div>
        </div>
    </form>
</div>

<!-- ================== GLOBAL VENDOR SCRIPTS ==================-->
<script src="{{ asset('assets/vendor/modernizr/modernizr.custom.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/vendor/js-storage/js.storage.js') }}"></script>
<script src="{{ asset('assets/vendor/js-cookie/src/js.cookie.js') }}"></script>
<script src="{{ asset('assets/vendor/pace/pace.js') }}"></script>
<script src="{{ asset('assets/vendor/metismenu/dist/metisMenu.js') }}"></script>
<script src="{{ asset('assets/vendor/switchery-npm/index.js') }}"></script>
<script src="{{ asset('assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<!-- ================== GLOBAL APP SCRIPTS ==================-->
<script src="{{ asset('assets/js/global/app.js') }}"></script>

</body>

</html>
