function removeClass() {
    let typeCategory = document.getElementById("typeCategory").value;
    let page = document.getElementById("categoryClass");
    let category = document.getElementById("pageClass");
    if (typeCategory === "category"){
        page.classList.remove('hide');
        category.classList.add('hide');
    }else{
        page.classList.add('hide');
        category.classList.remove('hide');
    }
}

function menuType() {
    let typeMenu = document.getElementById("typeMenu").value;
    let parentMenuClass = document.getElementById("parentMenuClass");
    if (typeMenu === "children"){
        parentMenuClass.classList.remove("hide");
    }else{
        parentMenuClass.classList.add("hide");
    }
}