<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Menu
 *
 * @property int $id
 * @property string $name
 * @property int $position
 * @property int|null $page_id
 * @property int|null $category_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Page $pages
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu wherePageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Menu whereType($value)
 * @property-read \App\Category|null $category
 */
class Menu extends Model
{
    protected $table = 'menus';

    protected $fillable = ['name', 'position', 'type', 'page_id', 'category_id'];

    public function pages()
    {
        return $this->hasOne(Page::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
