@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h3>{{ $budget->title }}</h3>
                    <br>
                    <p>{!! $budget->text !!}</p>
                    <hr>
                    <h3>Голосования</h3>
                    <input type="hidden" name="" id="budgetId" value="{{ $budget->id }}">
                    <div class="buttonClass" id="buttonId">
                        <button type="button" onclick="sendYes()" class="btn btn-success">За</button>
                        <button type="button" onclick="sendNo()" class="btn btn-danger">Против</button>
                        <button type="button" onclick="sendAbstain()" class="btn btn-warning">Воздержаться</button>
                    </div>
                    <div class="votes hide" id="votesClass">
                        <h3>Общий: <span id="idCount"></span></h3>
                        <h3>За: <span id="idYes"></span></h3>
                        <h3>Против: <span id="idNo"></span></h3>
                        <h3>Воздержаться: <span id="idAbstain"></span></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@push('page-js')
    <script>
        function sendYes() {
            let votesClass = document.getElementById('votesClass');
            let buttonClass = document.getElementById('buttonId');
            let id = document.getElementById('budgetId').value;

            votesClass.classList.remove('hide');
            buttonClass.classList.add('hide');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',

                url: '/profile/budget/show',

                data: {
                    vote: 'yes',
                    id: id
                },

                success: function (data) {
                    $('#idCount').text(data['allVotes']);
                    $('#idYes').text(data['yesVotes']);
                    $('#idNo').text(data['noVotes']);
                    $('#idAbstain').text(data['abstainVotes']);
                }
            });
        }
        
        function sendNo() {
            
        }
        
        function sendAbstain() {
            
        }
    </script>    
@endpush