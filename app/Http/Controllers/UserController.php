<?php

namespace App\Http\Controllers;

use App\Budget;
use App\Deputy;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    public function deputies()
    {
        $deputies = Deputy::all();
        return view('user.deputy', ['deputies' => $deputies]);
    }

    public function deputyView(Deputy $deputy)
    {
        return view('user.view_deputy', ['deputy' => $deputy]);
    }

    public function budgets()
    {
        $budgets = Budget::all();
        $data = [
            'i' => 1,
            'budgets' => $budgets
        ];
        return view('user.budget', $data);
    }

    public function budgetView(Budget $budget)
    {
        return view('user.view_budget', ['budget' => $budget]);
    }

    public function users()
    {
        $users = User::all();
        $data = [
            'users' => $users,
            'i' => 1
        ];
        return view('user.users', $data);
    }

    public function store(Request $request)
    {
        User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'pass' => $request->input('password')
        ]);
        return redirect()->route('users.index');
    }

    public function update(Request $request)
    {
        $user = User::findOrFail($request->input('user'));
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->pass = $request->input('password');
        $user->save();

        return redirect()->route('users.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        $user = User::findOrFail($request->input('user'));
        $user->delete();

        return redirect()->route('users.index');
    }

    public function addVotes()
    {
        $input = Input::get('vote');
        return response()->json($input);
    }
}
