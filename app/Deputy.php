<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Deputy
 *
 * @property int $id
 * @property string $fio
 * @property string $desc
 * @property string|null $image
 * @property string|null $info
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deputy whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deputy whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deputy whereFio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deputy whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deputy whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deputy whereInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Deputy whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Deputy extends Model
{
    protected $table = 'deputies';

    protected $fillable = ['fio', 'desc', 'image', 'info'];

    public static function getExcerpt($str, $startPos = 0, $maxLength = 180)
    {
        if (strlen($str) > $maxLength) {
            $excerpt = substr($str, $startPos, $maxLength - 6);
            $lastSpace = strrpos($excerpt, ' ');
            $excerpt = substr($excerpt, 0, $lastSpace);
            $excerpt .= ' ...';
        } else {
            $excerpt = $str;
        }

        return $excerpt;
    }
}
