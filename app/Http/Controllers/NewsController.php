<?php

namespace App\Http\Controllers;

use App\Category;
use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::all();
        return view('news.index', ['news' => $news]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('news.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->file('image') != null){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $fileName = str_random(5)."-".date('his')."-".str_random(3).".".$extension;
            $folderPath  = 'upload';
            $path = $file->move($folderPath , $fileName);
        }else{
            $path = null;
        }
        News::create([
            'title' => $request->input('title'),
            'desc'  => $request->input('desc'),
            'image' => $path,
            'category_id' => $request->input('category_id')
        ]);

        return redirect()->route('news.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        $categories = Category::all();
        $data = [
            'categories' => $categories,
            'news' => $news
        ];
        return view('news.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        if ($request->file('image') != null){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $fileName = str_random(5)."-".date('his')."-".str_random(3).".".$extension;
            $folderPath  = 'upload';
            $path = $file->move($folderPath , $fileName);
        }else{
            $path = $news->image;
        }
        $news->title = $request->input('title');
        $news->desc  = $request->input('desc');
        $news->image = $path;
        $news->category_id = $request->input('category_id');
        $news->save();

        return redirect()->route('news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        News::destroy($request->input('news'));
        return redirect()->route('news.index');
    }
}
