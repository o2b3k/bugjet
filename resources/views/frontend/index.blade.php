@extends('layouts.front')

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Государственный долг Кыргызской Республики</h5>
                <div class="card-body">
                    <canvas id="chartjs_barChart"></canvas>
                </div>
            </div>
        </div>
    </div>
@stop
@push('js')
    <script src="{{ asset('assets/js/charts/chartjs-init.js') }}"></script>
@endpush