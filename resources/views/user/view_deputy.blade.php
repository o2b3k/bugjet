@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h3>{{ $deputy->fio }}</h3>
                    <br>
                    <p>{!! $deputy->info !!}</p>
                </div>
            </div>
        </div>
    </div>
@stop