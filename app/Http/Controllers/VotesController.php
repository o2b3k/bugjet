<?php

namespace App\Http\Controllers;

use App\Budget;
use App\Votes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class VotesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $vote = Input::get('vote');
        $id = Input::get('id');
        $budget = Budget::findOrFail($id);
        $votes = new Votes();
        $votes->vote = $vote;
        $votes->budget_id = $budget->id;
        $votes->save();
        $all = count($budget->votes);
        $yes = Votes::whereBudgetId($id)->where('vote','=','yes')->get();
        $no = Votes::whereBudgetId($id)->where('vote','=','no')->get();
        $abstain = Votes::whereBudgetId($id)->where('vote','=','abstain')->get();
        return response()->json([
            'allVotes' => $all,
            'yesVotes' => count($yes),
            'noVotes' => count($no),
            'abstainVotes' => count($abstain)
        ]);
    }
}
