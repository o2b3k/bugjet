@extends('layouts.dashboard')
@section('title')
    Депутаты
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        @foreach($deputies as $deputy)
                            <div class="col-sm-12 col-md-6 col-xl-3">
                                <div class="card">
                                    <div class="card-content">
                                        <img class="card-img-top" src="{{ asset($deputy->image) }}">
                                        <div class="card-body">
                                            <h4 class="card-title">{{ $deputy->fio }}</h4>
                                            <p class="card-text">{{ $deputy->desc }}</p>
                                            <a href="{{ route('profile.deputy.view', ['deputy' => $deputy]) }}" class="btn btn-primary">Подробнее</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
