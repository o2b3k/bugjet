@extends('layouts.dashboard')
@section('title')
    Депутаты
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <a href="{{ route('deputy.create') }}" class="btn btn-success btn-floating btn-outline pull-right">
                        Добавить
                    </a>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Ф.И.О</th>
                                    <th>Инфо</th>
                                    <th>Действия</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($deputies as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->fio }}</td>
                                    <td>{!! \App\Deputy::getExcerpt($item->desc) !!}</td>
                                    <td>
                                        <a href="{{ route('deputy.edit', ['deputy' => $item]) }}" class="btn btn-warning">
                                            Изменить
                                        </a>
                                        <button type="button" class="btn btn-danger btn-outline"
                                                data-toggle="modal" data-target="#deleteDeputyModal"
                                                data-id="{{ $item->id }}"
                                                onclick="deleteDeputy(this)">
                                            Удалить
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteDeputyModal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('deputy.destroy') }}" method="post">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel9">Системное уведомление</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="zmdi zmdi-close"></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" name="deputy" id="deleteDeputy">
                            <p>Вы действительно хотите удалить?</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-outline" data-dismiss="modal">Закрыть</button>
                        <button type="submit" class="btn btn-danger">Удалить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@push('page-js')
    <script src="{{ asset('js/category.js') }}"></script>
@endpush