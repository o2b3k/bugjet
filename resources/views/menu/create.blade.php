@extends('layouts.dashboard')
@section('title')
    Добавить меню
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('menu.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="name">Наименования</label>
                                    <input type="text" name="name" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="position">Позиция</label>
                                    <input type="text" class="form-control" name="position">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="type">Тип</label>
                                    <select class="form-control" onchange="removeClass()" name="type" id="typeCategory">
                                        <option value="page">Страница</option>
                                        <option value="category">Категория</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group" id="pageClass">
                                    <label for="page_id">Страница</label>
                                    <select name="page_id" class="form-control">
                                            <option value=""></option>
                                        @foreach($pages as $page)
                                            <option value="{{ $page->id }}">{{ $page->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group hide" id="categoryClass">
                                    <label for="category_id">Категория</label>
                                    <select name="category_id" title="" class="form-control">
                                        <option value=""></option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <a href="{{ route('news.index') }}" class="btn btn-danger">Назад</a>
                        <button type="submit" class="btn btn-success pull-right">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@push('page-js')
    <script src="{{ asset('js/my.js') }}"></script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="{{ asset('js/tinymce.js') }}"></script>
@endpush