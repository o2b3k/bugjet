@extends('layouts.front')

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">{{ $news->title }}</h4>
            <p>{!! $news->desc !!}</p>
        </div>
    </div>
@stop