function edit(sender) {
    let id = $(sender).data('id');
    let name = $(sender).data('name');
    let email = $(sender).data('email');
    let pass = $(sender).data('pass');

    $('#editUserId').val(id);
    $('#userName').val(name);
    $('#userEmail').val(email);
    $('#userPassword').val(pass);
}

function destroy(sender) {
    let id = $(sender).data('id');
    $('#deleteUserId').val(id);
}