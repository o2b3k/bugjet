<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Budget
 *
 * @property int $id
 * @property string $title
 * @property string $text
 * @property string|null $file
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Budget whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Budget whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Budget whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Budget whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Budget whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Budget whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Votes $votes
 */
class Budget extends Model
{
    protected $table = 'budgets';

    protected $fillable = ['title', 'text', 'file'];

    public function votes()
    {
        return $this->hasMany(Votes::class);
    }
}