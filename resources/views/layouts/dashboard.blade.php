<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Бюджет</title>
    <!-- ================== GOOGLE FONTS ==================-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <!-- ======================= GLOBAL VENDOR STYLES ========================-->
    <link rel="stylesheet" href="{{ asset('assets/css/vendor/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/metismenu/dist/metisMenu.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/switchery-npm/index.css') }}">
    <link rel="stylesheet"
          href="{{ asset('assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css') }}">
    <!-- ======================= LINE AWESOME ICONS ===========================-->
    <link rel="stylesheet" href="{{ asset('assets/css/icons/line-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/icons/simple-line-icons.css') }}">
    <!-- ======================= DRIP ICONS ===================================-->
    <link rel="stylesheet" href="{{ asset('assets/css/icons/dripicons.min.css') }}">
    <!-- ======================= MATERIAL DESIGN ICONIC FONTS =================-->
    <link rel="stylesheet" href="{{ asset('assets/css/icons/material-design-iconic-font.min.css') }}">
    <!-- ======================= PAGE VENDOR STYLES ===========================-->
    <link rel="stylesheet" href="{{ asset('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
    <!-- ======================= GLOBAL COMMON STYLES ============================-->
    <link rel="stylesheet" href="{{ asset('assets/css/common/main.bundle.css') }}">
    <!-- ======================= LAYOUT TYPE ===========================-->
    <link rel="stylesheet" href="{{ asset('assets/css/layouts/vertical/core/main.css') }}">
    <!-- ======================= MENU TYPE ===========================-->
    <link rel="stylesheet" href="{{ asset('assets/css/layouts/vertical/menu-type/default.css') }}">
    <!-- ======================= THEME COLOR STYLES ===========================-->
    <link rel="stylesheet" href="{{ asset('assets/css/layouts/vertical/themes/theme-a.css') }}">
</head>
<body>
<!-- START APP WRAPPER -->
<div id="app">
    <!-- START MENU SIDEBAR WRAPPER -->
    <aside class="sidebar sidebar-left">
        <div class="sidebar-content">
            <div class="aside-toolbar">
                <ul class="site-logo">
                    <li>
                        <!-- START LOGO -->
                        <a href="{{ route('home') }}">
                            <span class="brand-text">Бюджет</span>
                        </a>
                        <!-- END LOGO -->
                    </li>
                </ul>
            </div>
            @include('layouts.main-menu')
        </div>
    </aside>
    <!-- END MENU SIDEBAR WRAPPER -->
    <div class="content-wrapper">
        <!-- START LOGO WRAPPER -->
        <nav class="top-toolbar navbar navbar-mobile navbar-tablet">
            <ul class="navbar-nav nav-left">
                <li class="nav-item">
                    <a href="javascript:void(0)" data-toggle-state="aside-left-open">
                        <i class="icon dripicons-align-left"></i>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav nav-center site-logo">
                <li>
                    <a href="{{ route('home') }}">
                        <span class="brand-text">Бюджет</span>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav nav-right">
                <li class="nav-item">
                    <a href="#">

                    </a>
                </li>
            </ul>
        </nav>
        <!-- END LOGO WRAPPER -->
        <div class="content">
            <!--START PAGE HEADER -->
            <header class="page-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h1>@yield('title')</h1>
                    </div>
                </div>
            </header>
            <!--END PAGE HEADER -->
            <!--START PAGE CONTENT -->
            <section class="page-content container-fluid">
                @yield('content')
            </section>
            <!--END PAGE CONTENT -->
        </div>
        <!-- SIDEBAR QUICK PANNEL WRAPPER -->
        @include('layouts.theme-changer')
        <!-- END SIDEBAR QUICK PANNEL WRAPPER -->
    </div>
</div>
<!-- END CONTENT WRAPPER -->
<!-- ================== GLOBAL VENDOR SCRIPTS ==================-->
<script src="{{ asset('assets/vendor/modernizr/modernizr.custom.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/vendor/js-storage/js.storage.js') }}"></script>
<script src="{{ asset('assets/vendor/js-cookie/src/js.cookie.js') }}"></script>
<script src="{{ asset('assets/vendor/pace/pace.js') }}"></script>
<script src="{{ asset('assets/vendor/metismenu/dist/metisMenu.js') }}"></script>
<script src="{{ asset('assets/vendor/switchery-npm/index.js') }}"></script>
<script src="{{ asset('assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<!-- ================== PAGE LEVEL VENDOR SCRIPTS ==================-->
@stack('page-vendor-js')
@yield('page-vendor-js')
<!-- ================== GLOBAL APP SCRIPTS ==================-->
<script src="{{ asset('assets/js/global/app.js') }}"></script>
<!-- ================== PAGE LEVEL SCRIPTS ==================-->
@stack('page-js')
@yield('page-js')
</body>
</html>
