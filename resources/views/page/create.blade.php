@extends('layouts.dashboard')
@section('title')
    Добавить страница
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('page.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name">Заголовок</label>
                            <input type="text" name="title" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Текст</label>
                            <textarea name="text" id="" cols="30" rows="10"
                                      class="form-control my-editor"></textarea>
                        </div>
                        <a href="{{ route('page.index') }}" class="btn btn-danger">Назад</a>
                        <button type="submit" class="btn btn-success pull-right">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@push('page-js')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="{{ asset('js/tinymce.js') }}"></script>
@endpush