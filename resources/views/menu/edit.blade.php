@extends('layouts.dashboard')
@section('title')
    Изменить меню
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('menu.update', ['menu' => $menu]) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="name">Наименования</label>
                                    <input type="text" name="name" class="form-control" value="{{ old('name', $menu->name) }}" required>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="position">Позиция</label>
                                    <input type="text" class="form-control" name="position" value="{{ old('position', $menu->position) }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="type">Тип</label>
                                    <select class="form-control" onchange="removeClass()" name="type" id="typeCategory">
                                        @if($menu->type == "page")
                                            <option value="page">Страница</option>
                                        @else
                                            <option value="category">Категория</option>
                                        @endif
                                        <option value="page">Страница</option>
                                        <option value="category">Категория</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group" id="pageClass">
                                    <label for="page_id">Страница</label>
                                    <select name="page_id" class="form-control">
                                        @if($menu->page_id)
                                            <option value="{{ $menu->page_id }}">{{ $menu->pages->name }}</option>
                                        @else
                                            <option value=""></option>
                                        @endif
                                        @foreach($pages as $page)
                                            <option value="{{ $page->id }}">{{ $page->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group hide" id="categoryClass">
                                    <label for="category_id">Категория</label>
                                    <select name="category_id" title="" class="form-control">
                                        @if($menu->category_id)
                                            <option value="{{ $menu->category_id }}">{{ $menu->category->name }}</option>
                                        @else
                                            <option value=""></option>
                                        @endif
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <a href="{{ route('news.index') }}" class="btn btn-danger">Назад</a>
                        <button type="submit" class="btn btn-success pull-right">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
@push('page-js')
    <script src="{{ asset('js/my.js') }}"></script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="{{ asset('js/tinymce.js') }}"></script>
@endpush