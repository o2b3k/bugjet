<?php

namespace App\Http\Controllers;

use App\Deputy;
use Illuminate\Http\Request;
use Image;

class DeputyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deputies = Deputy::all();
        return view('deputy.index', ['deputies' => $deputies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('deputy.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->file('image') != null){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $fileName = str_random(5)."-".date('his')."-".str_random(3).".".$extension;
            $folderPath  = 'upload';
            $path = $folderPath."/".$fileName;
            Image::make($file)->resize(800, 600)->save("$path");
        }else{
            $path = null;
        }
        Deputy::create([
            'fio' => $request->input('fio'),
            'desc'  => $request->input('desc'),
            'info' => $request->input('info'),
            'image' => $path,
        ]);
        return redirect()->route('deputy.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Deputy  $deputy
     * @return \Illuminate\Http\Response
     */
    public function show(Deputy $deputy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Deputy  $deputy
     * @return \Illuminate\Http\Response
     */
    public function edit(Deputy $deputy)
    {
        return view('deputy.edit', ['deputy' => $deputy]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Deputy  $deputy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Deputy $deputy)
    {
        if ($request->file('image') != null){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $fileName = str_random(5)."-".date('his')."-".str_random(3).".".$extension;
            $folderPath  = 'upload';
            $path = $file->move($folderPath , $fileName);
        }else{
            $path = $deputy->image;
        }
        $deputy->fio = $request->input('fio');
        $deputy->desc = $request->input('desc');
        $deputy->info = $request->input('info');
        $deputy->image = $path;
        $deputy->save();

        return redirect()->route('deputy.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Deputy::destroy($request->input('deputy'));
        return  redirect()->route('deputy.index');
    }
}
