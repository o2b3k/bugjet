<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','FrontendController@index')->name('index');
Route::get('page/{id}','FrontendController@viewPage')->name('view.page');
Route::get('category/{id}','FrontendController@viewCategory')->name('view.category');
Route::get('news/{id}','FrontendController@viewNews')->name('view.news');

Route::get('test', function (){
    return view('user.deputy');
});

Auth::routes();

Route::group(['middleware' => 'auth', 'prefix' => 'profile'], function (){
    Route::get('deputies','UserController@deputies')->name('profile.deputy');
    Route::get('deputy/show/{deputy}','UserController@deputyView')->name('profile.deputy.view');
    Route::get('budgets','UserController@budgets')->name('profile.budget');
    Route::get('budget/show/{budget}','UserController@budgetView')->name('profile.budget.view');
    Route::post('budget/show/','VotesController@store');
});

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function (){
    Route::get('home', 'HomeController@index')->name('home');
    Route::get('users','UserController@users')->name('users.index');
    Route::post('users','UserController@store')->name('users.store');
    Route::post('user/update','UserController@update')->name('users.update');
    Route::post('user/destroy','UserController@destroy')->name('users.destroy');

    /* Category routes */
    Route::get('category','CategoryController@index')->name('category.index');
    Route::post('category','CategoryController@store')->name('category.store');
    Route::post('category/edit','CategoryController@update')->name('category.update');
    Route::post('category/destroy','CategoryController@destroy')->name('category.destroy');
    /* News routes */
    Route::get('news','NewsController@index')->name('news.index');
    Route::get('news/create','NewsController@create')->name('news.create');
    Route::post('news/create','NewsController@store')->name('news.store');
    Route::get('news/edit/{news}','NewsController@edit')->name('news.edit');
    Route::post('news/edit/{news}','NewsController@update')->name('news.update');
    Route::post('news/destroy','NewsController@destroy')->name('news.destroy');
    /* Pages routes */
    Route::get('pages','PageController@index')->name('page.index');
    Route::get('pages/create','PageController@create')->name('page.create');
    Route::post('pages/create','PageController@store')->name('page.store');
    Route::get('pages/edit/{page}','PageController@edit')->name('page.edit');
    Route::post('pages/edit/{page}','PageController@update')->name('page.update');
    Route::post('pages/destroy','PageController@destroy')->name('page.destroy');
    /* Deputy routes */
    Route::get('deputy','DeputyController@index')->name('deputy.index');
    Route::get('deputy/create','DeputyController@create')->name('deputy.create');
    Route::post('deputy/create','DeputyController@store')->name('deputy.store');
    Route::get('deputy/edit/{deputy}','DeputyController@edit')->name('deputy.edit');
    Route::post('deputy/edit/{deputy}','DeputyController@update')->name('deputy.update');
    Route::post('deputy/destroy','DeputyController@destroy')->name('deputy.destroy');
    /* Budget routes */
    Route::get('budget','BudgetController@index')->name('budget.index');
    Route::get('budget/create','BudgetController@create')->name('budget.create');
    Route::post('budget/create','BudgetController@store')->name('budget.store');
    Route::get('budget/edit/{budget}','BudgetController@edit')->name('budget.edit');
    Route::post('budget/edit/{budget}','BudgetController@update')->name('budget.update');
    Route::post('budget/destroy','BudgetController@destroy')->name('budget.destroy');
    /* Menu routes */
    Route::get('menu','MenuController@index')->name('menu.index');
    Route::get('menu/create','MenuController@create')->name('menu.create');
    Route::post('menu/create','MenuController@store')->name('menu.store');
    Route::get('menu/edit/{menu}','MenuController@edit')->name('menu.edit');
    Route::post('menu/edit/{menu}','MenuController@update')->name('menu.update');
    Route::post('menu/destroy','MenuController@destroy')->name('menu.destroy');
});