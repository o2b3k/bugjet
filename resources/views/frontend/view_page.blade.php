@extends('layouts.front')

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">{{ $page->title }}</h4>
            <p>{!! $page->text !!}</p>
        </div>
    </div>
@stop